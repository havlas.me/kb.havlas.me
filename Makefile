.PHONY: build
build:
	mkdocs build -d public

.PHONY: dev
dev:
	mkdocs serve

.PHONY: clean
clean:
	rm --recursive public
