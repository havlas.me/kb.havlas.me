Grafana Dashboard Authoring
===========================

Color Scheme
------------

Yellow - `rgb(250, 204, 21)`  
Orange - `rgb(234, 88, 12)`  
Green - `rgb(22, 163, 74)`  
Red - `rgb(220, 38, 38)`  
Blue - `rgb(8, 145, 178)`  
Purple - `rgb(147, 51, 234)`  
