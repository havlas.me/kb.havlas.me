# Node Alert Rules

## Host Core

### Host Down

```yaml
- alert: HostDown
  expr: ( up{job="node"} ) < 1
  for: 30s
  labels:
    severity: critical
    priority: P2
  annotations:
    summary: {{ index $labels "instance" }}
    description: Host **{{ index $labels "instance" }}** has been down for the past 30 seconds.
```

```yaml
- alert: HostDown
  expr: ( up{job="node"} ) < 1
  for: 10m
  labels:
    severity: critical
    priority: P1
  annotations:
    summary: {{ index $labels "instance" }}
    description: Host **{{ index $labels "instance" }}** has been down for the past 10 minutes.
```

### Host Slow Scrape (>1s)

```yaml
- alert: HostSlowScrape
  expr: ( sum by (instance) (node_scrape_collector_duration_seconds{job="node"}) ) > 1
  for: 5m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} sec*
    description: Host **{{ index $labels "instance" }}** scrape takes longer than a second for the past 5 minutes.\nValue = {{ humanize (index $values "Value").Value }} sec
```

```yaml
- alert: HostSlowScrape
  expr: ( sum by (instance) (node_scrape_collector_duration_seconds{job="node"}) ) > 1
  for: 2h
  labels:
    severity: warning
    priority: P3
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} sec*
    description: Host **{{ index $labels "instance" }}** scrape takes longer than a second for the past 2 hours.\nValue = {{ humanize (index $values "Value").Value }} sec
```


## Host CPU

### Host High CPU Usage (>80%)

```yaml
- alert: HostHighCpuUsage
  expr: ( 100 - (sum by(instance) (rate(node_cpu_seconds_total{job="node",mode="idle"} [1m])) * 100 / on(instance) count by(instance) (count by(instance,cpu) (node_cpu_seconds_total{job="node"}))) ) > 80
  for: 30m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} % Used*
    description: Host **{{ index $labels "instance" }}** CPU usage has been above 80% for the past 30 minutes.\nValue = {{ humanize (index $values "Value").Value }} % Used
```

### Host Critical CPU Usage (>95%)

```yaml
- alert: HostCriticalCpuUsage
  expr: ( 100 - (sum by(instance) (rate(node_cpu_seconds_total{job="node",mode="idle"} [1m])) * 100 / on(instance) count by(instance) (count by(instance,cpu) (node_cpu_seconds_total{job="node"}))) ) > 95
  for: 5m
  labels:
    severity: critical
    priority: P2
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} % Used*
    description: Host **{{ index $labels "instance" }}** CPU usage has been above 95% for the past 5 minutes.\nValue = {{ humanize (index $values "Value").Value }} % Used
```

```yaml
- alert: HostCriticalCpuUsage
  expr: ( 100 - (sum by(instance) (rate(node_cpu_seconds_total{job="node",mode="idle"} [1m])) * 100 / on(instance) count by(instance) (count by(instance,cpu) (node_cpu_seconds_total{job="node"}))) ) > 95
  for: 2h
  labels:
    severity: critical
    priority: P1
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} % Used*
    description: Host **{{ index $labels "instance" }}** CPU usage has been above 95% for the past 2 hours.\nValue = {{ humanize (index $values "Value").Value }} % Used
```

### Host Anomalous CPU Usage (>3σ)

```yaml
- alert: HostAnomalousCpuUsage
  expr: abs( (sum by(instance) (avg_over_time(rate(node_cpu_seconds_total{job="node",mode="idle"} [1m]) [1m:])) - sum by(instance) (avg_over_time(rate(node_cpu_seconds_total{job="node",mode="idle"} [1m]) [7d:]))) / sum by(instance) (stddev_over_time(rate(node_cpu_seconds_total{job="node",mode="idle"} [1m]) [7d:])) ) > 3
  for: 15m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} σ*
    description: Host **{{ index $labels "instance" }}** CPU usage has been anomalous for the past 15 minutes.\nValue = {{ humanize (index $values "Value").Value }} σ
```

### Host High CPU Stall (>25%)

```yaml
- alert: HostHighCpuStall
  expr: ( rate(node_pressure_cpu_waiting_seconds_total{job="node"} [1m]) * 100 ) > 25
  for: 30m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} % Stalled*
    description: Host **{{ index $labels "instance" }}** CPU has been stalled more than 25% of time for the past 30 minutes.\nValue = {{ humanize (index $values "Value").Value }} % Stalled
```

### Host Critical CPU Stall (>60%)

```yaml
- alert: HostCriticalCpuStall
  expr: ( rate(node_pressure_cpu_waiting_seconds_total{job="node"} [1m]) * 100 ) > 60
  for: 5m
  labels:
    severity: critical
    priority: P2
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} % Stalled*
    description: Host **{{ index $labels "instance" }}** CPU has been stalled more than 60% of time for the past 5 minutes.\nValue = {{ humanize (index $values "Value").Value }} % Stalled
```

```yaml
- alert: HostCriticalCpuStall
  expr: ( rate(node_pressure_cpu_waiting_seconds_total{job="node"} [1m]) * 100 ) > 60
  for: 2h
  labels:
    severity: critical
    priority: P1
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} % Stalled*
    description: Host **{{ index $labels "instance" }}** CPU has been stalled more than 60% of time for the past 2 hours.\nValue = {{ humanize (index $values "Value").Value }} % Stalled
```


## Host Memory

### Host High RAM Usage (>80%)

!!! note "Condition"

    `and node_memory_MemAvailable_bytes{job="node"} / 1024 / 1024 < 1024`

```yaml
- alert: HostHighRamUsage
  expr: ( 100 - (node_memory_MemAvailable_bytes{job="node"} * 100 / node_memory_MemTotal_bytes{job="node"}) ) > 80 and node_memory_MemAvailable_bytes{job="node"} / 1024 / 1024 < 1024
  for: 30m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} % Used*
    description: Host **{{ index $labels "instance" }}** RAM usage is above 80% for the past 30 minutes.\nValue = {{ humanize (index $values "Value").Value }} % Used
```

### Host Critical RAM Usage (>95%)

```yaml
- alert: HostCriticalRamUsage
  expr: ( 100 - (node_memory_MemAvailable_bytes{job="node"} * 100 / node_memory_MemTotal_bytes{job="node"}) ) > 95
  for: 5m
  labels:
    severity: critical
    priority: P2
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} % Used*
    description: Host **{{ index $labels "instance" }}** RAM usage is above 95% for the past 5 minutes.\nValue = {{ humanize (index $values "Value").Value }} % Used
```

```yaml
- alert: HostCriticalRamUsage
  expr: ( 100 - (node_memory_MemAvailable_bytes{job="node"} * 100 / node_memory_MemTotal_bytes{job="node"}) ) > 95
  for: 2h
  labels:
    severity: critical
    priority: P1
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} % Used*
    description: Host **{{ index $labels "instance" }}** RAM usage is above 95% for the past 2 hours.\nValue = {{ humanize (index $values "Value").Value }} % Used
```

### Host Anomalous RAM Usage (>3σ)

```yaml
- alert: HostAnomalousRamUsage
  expr: abs( (avg_over_time(node_memory_MemAvailable_bytes{job="node"} [1m]) - avg_over_time(node_memory_MemAvailable_bytes{job="node"} [7d])) / stddev_over_time(node_memory_MemAvailable_bytes{job="node"} [7d]) ) > 3
  for: 15m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} σ*
    description: Host **{{ index $labels "instance" }}** RAM usage has been anomalous for the past 15 minutes.\nValue = {{ humanize (index $values "Value").Value }} σ
```

### Host High SWAP Usage (>25%)

!!! note "Condition"

    `and node_memory_SwapTotal_bytes{job="node"} > 0`

```yaml
- alert: HostHighSwapUsage
  expr: ( 100 - (node_memory_SwapFree_bytes{job="node"} * 100 / node_memory_SwapTotal_bytes{job="node"}) ) > 25 and node_memory_SwapTotal_bytes{job="node"} > 0
  for: 30m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} % Used*
    description: Host **{{ index $labels "instance" }}** SWAP usage is above 25% for the past 30 minutes.\nValue = {{ humanize (index $values "Value").Value }} % Used
```

### Host Critical SWAP Usage (>90%)

!!! note "Condition"

    `and node_memory_SwapTotal_bytes{job="node"} > 0`

```yaml
- alert: HostCriticalSwapUsage
  expr: ( 100 - (node_memory_SwapFree_bytes{job="node"} * 100 / node_memory_SwapTotal_bytes{job="node"}) ) > 90 and node_memory_SwapTotal_bytes{job="node"} > 0
  for: 5m
  labels:
    severity: critical
    priority: P2
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} % Used*
    description: Host **{{ index $labels "instance" }}** SWAP usage is above 90% for the past 5 minutes.\nValue = {{ humanize (index $values "Value").Value }} % Used
```

```yaml
- alert: HostCriticalSwapUsage
  expr: ( 100 - (node_memory_SwapFree_bytes{job="node"} * 100 / node_memory_SwapTotal_bytes{job="node"}) ) > 90 and node_memory_SwapTotal_bytes{job="node"} > 0
  for: 2h
  labels:
    severity: critical
    priority: P1
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} % Used*
    description: Host **{{ index $labels "instance" }}** SWAP usage is above 90% for the past 2 hours.\nValue = {{ humanize (index $values "Value").Value }} % Used
```

### Host Anomalous Memory Read (>3σ)

```yaml
- alert: HostAnomalousMemoryRead
  expr: abs( (avg_over_time(rate(node_vmstat_pgpgout{job="node"} [1m]) [1m:]) - avg_over_time(rate(node_vmstat_pgpgout{job="node"} [1m]) [24h:])) / stddev_over_time(rate(node_vmstat_pgpgout{job="node"} [1m]) [24h:]) ) > 3
  for: 15m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} σ*
    description: Host **{{ index $labels "instance" }}** memory read has been anomalous for the past 15 minutes.\nValue = {{ humanize (index $values "Value").Value }} σ
```

### Host Anomalous Memory Write (>3σ)

```yaml
- alert: HostAnomalousMemoryWrite
  expr: abs( (avg_over_time(rate(node_vmstat_pgpgin{job="node"} [1m]) [1m:]) - avg_over_time(rate(node_vmstat_pgpgin{job="node"} [1m]) [24h:])) / stddev_over_time(rate(node_vmstat_pgpgin{job="node"} [1m]) [24h:]) ) > 3
  for: 15m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} σ*
    description: Host **{{ index $labels "instance" }}** memory write has been anomalous for the past 15 minutes.\nValue = {{ humanize (index $values "Value").Value }} σ
```

### Host High Memory Stall (>25%)

```yaml
- alert: HostHighMemoryStall
  expr: ( rate(node_pressure_memory_waiting_seconds_total{job="node"} [1m]) * 100 ) > 25
  for: 30m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} % Stalled*
    description: Host **{{ index $labels "instance" }}** memory is stalled more than 25% of time for the past 30 minutes.\nValue = {{ humanize (index $values "Value").Value }} % Stalled
```

### Host Critical Memory Stall (>60%)

```yaml
- alert: HostCriticalMemoryStall
  expr: ( rate(node_pressure_memory_waiting_seconds_total{job="node"} [1m]) * 100 ) > 60
  for: 5m
  labels:
    severity: critical
    priority: P2
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} % Stalled*
    description: Host **{{ index $labels "instance" }}** memory is stalled more than 60% of time for the past 5 minutes.\nValue = {{ humanize (index $values "Value").Value }} % Stalled
```

```yaml
- alert: HostCriticalMemoryStall
  expr: ( rate(node_pressure_memory_waiting_seconds_total{job="node"} [1m]) * 100 ) > 60
  for: 2h
  labels:
    severity: critical
    priority: P1
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} % Stalled*
    description: Host **{{ index $labels "instance" }}** memory is stalled more than 60% of time for the past 2 hours.\nValue = {{ humanize (index $values "Value").Value }} % Stalled
```

### Host High Memory Fault Rate (>10/sec)

```yaml
- alert: HostHighMemoryFaultRate
  expr: ( rate(node_vmstat_pgmajfault{job="node"} [1m]) ) > 10
  for: 30m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} #/sec*
    description: Host **{{ index $labels "instance" }}** memory has high fault rate for the past 30 minutes.\nValue = {{ humanize (index $values "Value").Value }} #/sec
```

### Host Critical Memory Fault Rate (>1000/sec)

```yaml
- alert: HostCriticalMemoryFaultRate
  expr: ( rate(node_vmstat_pgmajfault{job="node"} [1m]) ) > 1000
  for: 1m
  labels:
    severity: critical
    priority: P1
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} #/sec*
    description: Host **{{ index $labels "instance" }}** memory has critical fault rate for the past 1 minute.\nValue = {{ humanize (index $values "Value").Value }} #/sec
```


## Host I/O

### Host High I/O Stall (>25%)

```yaml
- alert: HostHighIoStall
  expr: ( rate(node_pressure_io_waiting_seconds_total{job="node"} [1m]) * 100 ) > 25
  for: 30m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} % Stalled*
    description: Host **{{ index $labels "instance" }}** I/O is stalled more than 25% of time for the past 30 minutes.\nValue = {{ humanize (index $values "Value").Value }} % Stalled
```

### Host Critical I/O Stall (>60%)

```yaml
- alert: HostCriticalIoStall
  expr: ( rate(node_pressure_io_waiting_seconds_total{job="node"} [1m]) * 100 ) > 60
  for: 5m
  labels:
    severity: critical
    priority: P2
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} % Stalled*
    description: Host **{{ index $labels "instance" }}** I/O is stalled more than 60% of time for the past 5 minutes.\nValue = {{ humanize (index $values "Value").Value }} % Stalled
```

```yaml
- alert: HostCriticalIoStall
  expr: ( rate(node_pressure_io_waiting_seconds_total{job="node"} [1m]) * 100 ) > 60
  for: 2h
  labels:
    severity: critical
    priority: P1
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} % Stalled*
    description: Host **{{ index $labels "instance" }}** I/O is stalled more than 60% of time for the past 2 hours.\nValue = {{ humanize (index $values "Value").Value }} % Stalled
```


## Host Disk

### Host Low Disk Space (<10%)

!!! note "Condition"

    `and node_filesystem_avail_bytes{job="node",device!="rootfs",fstype=~"ext."} / 1024 / 1024 / 1024 < 512`

```yaml
- alert: HostLowDiskSpace
  expr: ( node_filesystem_avail_bytes{job="node",device!="rootfs",fstype=~"ext."} * 100 / node_filesystem_size_bytes{job="node",device!="rootfs",fstype=~"ext."} ) < 10 and node_filesystem_avail_bytes{job="node",device!="rootfs",fstype=~"ext."} / 1024 / 1024 / 1024 < 512
  for: 15m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "mountpoint" }}  »  *{{ humanize (index $values "Value").Value }} % Free*
    description: Host **{{ index $labels "instance" }}** disk **{{ index $labels "mountpoint" }}** free space is below 10% for the past 15 minutes.\nValue = {{ humanize (index $values "Value").Value }} %
```

### Host Critical Disk Space (<1%)

```yaml
- alert: HostCriticalDiskSpace
  expr: ( node_filesystem_avail_bytes{job="node",device!="rootfs",fstype=~"ext."} * 100 / node_filesystem_size_bytes{job="node",device!="rootfs",fstype=~"ext."} ) < 1
  for: 5m
  labels:
    severity: critical
    priority: P2
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "mountpoint" }}  »  *{{ humanize (index $values "Value").Value }} % Free*
    description: Host **{{ index $labels "instance" }}** disk **{{ index $labels "mountpoint" }}** free space is below 1% for the past 5 minutes.\nValue = {{ humanize (index $values "Value").Value }} %
```

```yaml
- alert: HostCriticalDiskSpace
  expr: ( node_filesystem_avail_bytes{job="node",device!="rootfs",fstype=~"ext."} * 100 / node_filesystem_size_bytes{job="node",device!="rootfs",fstype=~"ext."} ) < 1
  for: 2h
  labels:
    severity: critical
    priority: P1
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "mountpoint" }}  »  *{{ humanize (index $values "Value").Value }} % Free*
    description: Host **{{ index $labels "instance" }}** disk **{{ index $labels "mountpoint" }}** free space is below 1% for the past 2 hours.\nValue = {{ humanize (index $values "Value").Value }} %
```

### Host Critical Disk Space (<1GB)

```yaml
- alert: HostCriticalDiskSpace
  expr: ( node_filesystem_avail_bytes{job="node",device!="rootfs",fstype=~"ext.",mountpoint!="/boot"} / 1024 / 1024 ) < 1024
  for: 5m
  labels:
    severity: critical
    priority: P2
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "mountpoint" }}  »  *{{ humanize (index $values "Value").Value }} MB*
    description: Host **{{ index $labels "instance" }}** disk **{{ index $labels "mountpoint" }}** free space is below 1GB for the past 5 minutes.\nValue = {{ humanize (index $values "Value").Value }} MB
```

```yaml
- alert: HostCriticalDiskSpace
  expr: ( node_filesystem_avail_bytes{job="node",device!="rootfs",fstype=~"ext.",mountpoint!="/boot"} / 1024 / 1024 ) < 1024
  for: 2h
  labels:
    severity: critical
    priority: P1
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "mountpoint" }}  »  *{{ humanize (index $values "Value").Value }} MB*
    description: Host **{{ index $labels "instance" }}** disk **{{ index $labels "mountpoint" }}** free space is below 1GB for the past 2 hours.\nValue = {{ humanize (index $values "Value").Value }} MB
```

### Host Low File Nodes (<20%)

```yaml
- alert: HostLowFileNodes
  expr: ( node_filesystem_files_free{job="node",device!="rootfs",fstype=~"ext."} * 100 / node_filesystem_files{job="node",device!="rootfs",fstype=~"ext."} ) < 20
  for: 15m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} % Free*
    description: Host **{{ index $labels "instance" }}** disk **{{ index $labels "mountpoint" }}** free file nodes(inodes) is below 20% for the past 15 minutes.\nValue = {{ humanize (index $values "Value").Value }} % Free
```

### Host Critical File Nodes (<1%)

```yaml
- alert: HostCriticalFileNodes
  expr: ( node_filesystem_files_free{job="node",device!="rootfs",fstype=~"ext."} * 100 / node_filesystem_files{job="node",device!="rootfs",fstype=~"ext."} ) < 1
  for: 5m
  labels:
    severity: critical
    priority: P2
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} % Free*
    description: Host **{{ index $labels "instance" }}** disk **{{ index $labels "mountpoint" }}** free file nodes(inodes) is below 1% for the past 5 minutes.\nValue = {{ humanize (index $values "Value").Value }} % Free
```

```yaml
- alert: HostCriticalFileNodes
  expr: ( node_filesystem_files_free{job="node",device!="rootfs",fstype=~"ext."} * 100 / node_filesystem_files{job="node",device!="rootfs",fstype=~"ext."} ) < 1
  for: 2h
  labels:
    severity: critical
    priority: P1
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} % Free*
    description: Host **{{ index $labels "instance" }}** disk **{{ index $labels "mountpoint" }}** free file nodes(inodes) is below 1% for the past 2 hours.\nValue = {{ humanize (index $values "Value").Value }} % Free
```

### Host No Disk Space Soon (<7d)

```yaml
- alert: HostNoDiskSpaceSoon
  expr: ( predict_linear(node_filesystem_avail_bytes{job="node",device!="rootfs",fstype=~"ext."}[24h], 7*24*3600) ) < 0
  for: 30m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "mountpoint" }}
    description: Host **{{ index $labels "instance" }}** disk **{{ index $labels "mountpoint" }}** is predicted to be full in the next 7 days.
```

### Host No Disk Space Soon (<24h)

```yaml
- alert: HostNoDiskSpaceSoon
  expr: ( predict_linear(node_filesystem_avail_bytes{job="node",device!="rootfs",fstype=~"ext."}[24h], 24*3600) ) < 0
  for: 30m
  labels:
    severity: warning
    priority: P3
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "mountpoint" }}
    description: Host **{{ index $labels "instance" }}** disk **{{ index $labels "mountpoint" }}** is predicted to be full in the next 24 hours.
```


## Host Network

### Host High Network Transmit Usage (>80%)

!!! note "Condition"

    `and node_network_speed_bytes{device!="lo"} > 0`

```yaml
- alert: HostHighNetworkTransmitUsage
  expr: ( (rate(node_network_transmit_bytes_total{device!="lo"} [1m]) * 100 / node_network_speed_bytes{device!="lo"}) ) > 80 and node_network_speed_bytes{device!="lo"} > 0
  for: 30m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "device" }}  »  *{{ humanize (index $values "Value").Value }} % Used*
    description: Host **{{ index $labels "instance" }}** network interface **{{ index $labels "device" }}** transmit usage is above 80% for the past 30 minutes.\nValue = {{ humanize (index $values "Value").Value }} % Used
```

### Host Critical Network Transmit Usage (>95%)

!!! note "Condition"

    `and node_network_speed_bytes{device!="lo"} > 0`

```yaml
- alert: HostCriticalNetworkTransmitUsage
  expr: ( (rate(node_network_transmit_bytes_total{device!="lo"} [1m]) * 100 / node_network_speed_bytes{device!="lo"}) ) > 95 and node_network_speed_bytes{device!="lo"} > 0
  for: 5m
  labels:
    severity: critical
    priority: P2
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "device" }}  »  *{{ humanize (index $values "Value").Value }} % Used*
    description: Host **{{ index $labels "instance" }}** network interface **{{ index $labels "device" }}** transmit usage is above 95% for the past 5 minutes.\nValue = {{ humanize (index $values "Value").Value }} % Used
```

```yaml
- alert: HostCriticalNetworkTransmitUsage
  expr: ( (rate(node_network_transmit_bytes_total{device!="lo"} [1m]) * 100 / node_network_speed_bytes{device!="lo"}) ) > 95 and node_network_speed_bytes{device!="lo"} > 0
  for: 2h
  labels:
    severity: critical
    priority: P1
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "device" }}  »  *{{ humanize (index $values "Value").Value }} % Used*
    description: Host **{{ index $labels "instance" }}** network interface **{{ index $labels "device" }}** transmit usage is above 95% for the past 2 hours.\nValue = {{ humanize (index $values "Value").Value }} % Used
```

### Host Anomalous Network Transmit Usage (>3σ)

!!! note "Condition"

    `and node_network_speed_bytes{device!="lo"}`

```yaml
- alert: HostAnomalousNetworkTransmitUsage
  expr: abs( (avg_over_time(rate(node_network_transmit_bytes_total{device!="lo"} [1m]) [1m:]) - avg_over_time(rate(node_network_transmit_bytes_total{device!="lo"} [1m]) [7d:])) / stddev_over_time(rate(node_network_transmit_bytes_total{device!="lo"} [1m]) [7d:]) ) > 3 and node_network_speed_bytes{device!="lo"}
  for: 15m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "device" }}  »  *{{ humanize (index $values "Value").Value }} σ*
    description: Host **{{ index $labels "instance" }}** network interface **{{ index $labels "device" }}** transmit usage has been anomalous for the past 15 minutes.\nValue = {{ humanize (index $values "Value").Value }} σ
```

```yaml
- alert: HostAnomalousNetworkTransmitUsage
  expr: abs( (avg_over_time(rate(node_network_transmit_packets_total{device!="lo"} [1m]) [1m:]) - avg_over_time(rate(node_network_transmit_packets_total{device!="lo"} [1m]) [7d:])) / stddev_over_time(rate(node_network_transmit_packets_total{device!="lo"} [1m]) [7d:]) ) > 3 and node_network_speed_bytes{device!="lo"}
  for: 15m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "device" }}  »  *{{ humanize (index $values "Value").Value }} σ*
    description: Host **{{ index $labels "instance" }}** network interface **{{ index $labels "device" }}** transmit usage has been anomalous for the past 15 minutes.\nValue = {{ humanize (index $values "Value").Value }} σ
```

### Host High Network Receive Usage (>80%)

!!! note "Condition"

    `and node_network_speed_bytes{device!="lo"} > 0`

```yaml
- alert: HostHighNetworkReceiveUsage
  expr: ( (rate(node_network_receive_bytes_total{device!="lo"} [1m]) * 100 / node_network_speed_bytes{device!="lo"}) ) > 80 and node_network_speed_bytes{device!="lo"} > 0
  for: 30m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "device" }}  »  *{{ humanize (index $values "Value").Value }} % Used*
    description: Host **{{ index $labels "instance" }}** network interface **{{ index $labels "device" }}** receive usage is above 80% for the past 30 minutes.\nValue = {{ humanize (index $values "Value").Value }} % Used
```

### Host Critical Network Receive Usage (>95%)

!!! note "Condition"

    `and node_network_speed_bytes{device!="lo"} > 0`

```yaml
- alert: HostCriticalNetworkReceiveUsage
  expr: ( (rate(node_network_receive_bytes_total{device!="lo"} [1m]) * 100 / node_network_speed_bytes{device!="lo"}) ) > 95 and node_network_speed_bytes{device!="lo"} > 0
  for: 5m
  labels:
    severity: critical
    priority: P2
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "device" }}  »  *{{ humanize (index $values "Value").Value }} % Used*
    description: Host **{{ index $labels "instance" }}** network interface **{{ index $labels "device" }}** receive usage is above 95% for the past 5 minutes.\nValue = {{ humanize (index $values "Value").Value }} % Used
```

```yaml
- alert: HostCriticalNetworkReceiveUsage
  expr: ( (rate(node_network_receive_bytes_total{device!="lo"} [1m]) * 100 / node_network_speed_bytes{device!="lo"}) ) > 95 and node_network_speed_bytes{device!="lo"} > 0
  for: 2h
  labels:
    severity: critical
    priority: P1
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "device" }}  »  *{{ humanize (index $values "Value").Value }} % Used*
    description: Host **{{ index $labels "instance" }}** network interface **{{ index $labels "device" }}** receive usage is above 95% for the past 2 hours.\nValue = {{ humanize (index $values "Value").Value }} % Used
```

### Host Anomalous Network Receive Usage (>3σ)

!!! note "Condition"

    `and node_network_speed_bytes{device!="lo"}`

```yaml
- alert: HostAnomalousNetworkReceiveUsage
  expr: abs( (avg_over_time(rate(node_network_receive_bytes_total{device!="lo"} [1m]) [1m:]) - avg_over_time(rate(node_network_receive_bytes_total{device!="lo"} [1m]) [7d:])) / stddev_over_time(rate(node_network_receive_bytes_total{device!="lo"} [1m]) [7d:]) ) > 3 and node_network_speed_bytes{device!="lo"}
  for: 15m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "device" }}  »  *{{ humanize (index $values "Value").Value }} σ*
    description: Host **{{ index $labels "instance" }}** network interface **{{ index $labels "device" }}** receive usage has been anomalous for the past 15 minutes.\nValue = {{ humanize (index $values "Value").Value }} σ
```

```yaml
- alert: HostAnomalousNetworkReceiveUsage
  expr: abs( (avg_over_time(rate(node_network_receive_packets_total{device!="lo"} [1m]) [1m:]) - avg_over_time(rate(node_network_receive_packets_total{device!="lo"} [1m]) [7d:])) / stddev_over_time(rate(node_network_receive_packets_total{device!="lo"} [1m]) [7d:]) ) > 3 and node_network_speed_bytes{device!="lo"}
  for: 15m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "device" }}  »  *{{ humanize (index $values "Value").Value }} σ*
    description: Host **{{ index $labels "instance" }}** network interface **{{ index $labels "device" }}** receive usage has been anomalous for the past 15 minutes.\nValue = {{ humanize (index $values "Value").Value }} σ
```

### Host High Network Transmit Drop (>1%)

!!! note "Condition"

    `and node_network_speed_bytes{device!="lo"}`

```yaml
- alert: HostHighNetworkTransmitDrop
  expr: ( rate(node_network_transmit_drop_total{device!="lo"} [1m]) * 100 / rate(node_network_transmit_packets_total{device!="lo"} [1m]) ) > 1 and ( rate(node_network_transmit_drop_total{device!="lo"} [1m]) ) > 1 and node_network_speed_bytes{device!="lo"}
  for: 15m
  labels:
    severity: warning
    priority: P3
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "device" }}  »  *{{ humanize (index $values "Value").Value }} % Drop*
    description: Host **{{ index $labels "instance" }}** network interface **{{ index $labels "device" }}** has encountered transmit drop.\nValue = {{ humanize (index $values "Value").Value }} % Drop
```

### Host Critical Network Transmit Drop (>10%)

!!! note "Condition"

    `and node_network_speed_bytes{device!="lo"}`

```yaml
- alert: HostCriticalNetworkTransmitDrop
  expr: ( rate(node_network_transmit_drop_total{device!="lo"} [1m]) * 100 / rate(node_network_transmit_packets_total{device!="lo"} [1m]) ) > 10 and ( rate(node_network_transmit_drop_total{device!="lo"} [1m]) ) > 1 and node_network_speed_bytes{device!="lo"}
  for: 5m
  labels:
    severity: critical
    priority: P2
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "device" }}  »  *{{ humanize (index $values "Value").Value }} % Drop*
    description: Host **{{ index $labels "instance" }}** network interface **{{ index $labels "device" }}** has encountered transmit drop.\nValue = {{ humanize (index $values "Value").Value }} % Drop
```

```yaml
- alert: HostCriticalNetworkReceiveDrop
  expr: ( rate(node_network_transmit_drop_total{device!="lo"} [1m]) * 100 / rate(node_network_transmit_packets_total{device!="lo"} [1m]) ) > 10 and ( rate(node_network_transmit_drop_total{device!="lo"} [1m]) ) > 1 and node_network_speed_bytes{device!="lo"}
  for: 2h
  labels:
    severity: critical
    priority: P1
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "device" }}  »  *{{ humanize (index $values "Value").Value }} % Drop*
    description: Host **{{ index $labels "instance" }}** network interface **{{ index $labels "device" }}** has encountered transmit drop.\nValue = {{ humanize (index $values "Value").Value }} % Drop
```

### Host High Network Receive Drop (>1%)

!!! note "Condition"

    `and node_network_speed_bytes{device!="lo"}`

```yaml
- alert: HostHighNetworkReceiveDrop
  expr: ( rate(node_network_receive_drop_total{device!="lo"} [1m]) * 100 / rate(node_network_receive_packets_total{device!="lo"} [1m]) ) > 1 and ( rate(node_network_receive_drop_total{device!="lo"} [1m]) ) > 1 and node_network_speed_bytes{device!="lo"}
  for: 15m
  labels:
    severity: warning
    priority: P3
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "device" }}  »  *{{ humanize (index $values "Value").Value }} % Drop*
    description: Host **{{ index $labels "instance" }}** network interface **{{ index $labels "device" }}** has encountered receive drop.\nValue = {{ humanize (index $values "Value").Value }} % Drop
```

### Host Critical Network Receive Drop (>10%)

!!! note "Condition"

    `and node_network_speed_bytes{device!="lo"}`

```yaml
- alert: HostCriticalNetworkReceiveDrop
  expr: ( rate(node_network_receive_drop_total{device!="lo"} [1m]) * 100 / rate(node_network_receive_packets_total{device!="lo"} [1m]) ) > 10 and ( rate(node_network_receive_drop_total{device!="lo"} [1m]) ) > 1 and node_network_speed_bytes{device!="lo"}
  for: 5m
  labels:
    severity: critical
    priority: P2
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "device" }}  »  *{{ humanize (index $values "Value").Value }} % Drop*
    description: Host **{{ index $labels "instance" }}** network interface **{{ index $labels "device" }}** has encountered receive drop.\nValue = {{ humanize (index $values "Value").Value }} % Drop
```

```yaml
- alert: HostCriticalNetworkReceiveDrop
  expr: ( rate(node_network_receive_drop_total{device!="lo"} [1m]) * 100 / rate(node_network_receive_packets_total{device!="lo"} [1m]) ) > 10 and ( rate(node_network_receive_drop_total{device!="lo"} [1m]) ) > 1 and node_network_speed_bytes{device!="lo"}
  for: 2h
  labels:
    severity: critical
    priority: P1
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "device" }}  »  *{{ humanize (index $values "Value").Value }} % Drop*
    description: Host **{{ index $labels "instance" }}** network interface **{{ index $labels "device" }}** has encountered receive drop.\nValue = {{ humanize (index $values "Value").Value }} % Drop
```

### Host High Network Transmit Error (>1%)

!!! note "Condition"

    `and node_network_speed_bytes{device!="lo"}`

```yaml
- alert: HostHighNetworkTransmitError
  expr: ( rate(node_network_transmit_errs_total{device!="lo"} [1m]) * 100 / rate(node_network_transmit_packets_total{device!="lo"} [1m]) ) > 1 and ( rate(node_network_transmit_errs_total{device!="lo"} [1m]) ) > 1 and node_network_speed_bytes{device!="lo"}
  for: 15m
  labels:
    severity: warning
    priority: P3
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "device" }}  »  *{{ humanize (index $values "Value").Value }} % Error*
    description: Host **{{ index $labels "instance" }}** network interface **{{ index $labels "device" }}** has encountered transmit errors.\nValue = {{ humanize (index $values "Value").Value }} % Error
```

### Host Critical Network Transmit Error (>10%)

!!! note "Condition"

    `and node_network_speed_bytes{device!="lo"}`

```yaml
- alert: HostCriticalNetworkTransmitError
  expr: ( rate(node_network_transmit_errs_total{device!="lo"} [1m]) * 100 / rate(node_network_transmit_packets_total{device!="lo"} [1m]) ) > 10 and ( rate(node_network_transmit_errs_total{device!="lo"} [1m]) ) > 1 and node_network_speed_bytes{device!="lo"}
  for: 5m
  labels:
    severity: critical
    priority: P2
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "device" }}  »  *{{ humanize (index $values "Value").Value }} % Error*
    description: Host **{{ index $labels "instance" }}** network interface **{{ index $labels "device" }}** has encountered transmit errors.\nValue = {{ humanize (index $values "Value").Value }} % Error
```

```yaml
- alert: HostCriticalNetworkReceiveError
  expr: ( rate(node_network_transmit_errs_total{device!="lo"} [1m]) * 100 / rate(node_network_transmit_packets_total{device!="lo"} [1m]) ) > 10 and ( rate(node_network_transmit_errs_total{device!="lo"} [1m]) ) > 1 and node_network_speed_bytes{device!="lo"}
  for: 2h
  labels:
    severity: critical
    priority: P1
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "device" }}  »  *{{ humanize (index $values "Value").Value }} % Error*
    description: Host **{{ index $labels "instance" }}** network interface **{{ index $labels "device" }}** has encountered transmit errors.\nValue = {{ humanize (index $values "Value").Value }} % Error
```

### Host High Network Receive Error (>1%)

!!! note "Condition"

    `and node_network_speed_bytes{device!="lo"}`

```yaml
- alert: HostHighNetworkReceiveError
  expr: ( rate(node_network_receive_errs_total{device!="lo"} [1m]) * 100 / rate(node_network_receive_packets_total{device!="lo"} [1m]) ) > 1 and ( rate(node_network_receive_errs_total{device!="lo"} [1m]) ) > 1 and node_network_speed_bytes{device!="lo"}
  for: 15m
  labels:
    severity: warning
    priority: P3
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "device" }}  »  *{{ humanize (index $values "Value").Value }} % Error*
    description: Host **{{ index $labels "instance" }}** network interface **{{ index $labels "device" }}** has encountered receive errors.\nValue = {{ humanize (index $values "Value").Value }} % Error
```

### Host Critical Network Receive Error (>10%)

!!! note "Condition"

    `and node_network_speed_bytes{device!="lo"}`

```yaml
- alert: HostCriticalNetworkReceiveError
  expr: ( rate(node_network_receive_errs_total{device!="lo"} [1m]) * 100 / rate(node_network_receive_packets_total{device!="lo"} [1m]) ) > 10 and ( rate(node_network_receive_errs_total{device!="lo"} [1m]) ) > 1 and node_network_speed_bytes{device!="lo"}
  for: 5m
  labels:
    severity: critical
    priority: P2
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "device" }}  »  *{{ humanize (index $values "Value").Value }} % Error*
    description: Host **{{ index $labels "instance" }}** network interface **{{ index $labels "device" }}** has encountered receive errors.\nValue = {{ humanize (index $values "Value").Value }} % Error
```

```yaml
- alert: HostCriticalNetworkReceiveError
  expr: ( rate(node_network_transmit_errs_total{device!="lo"} [1m]) * 100 / rate(node_network_transmit_packets_total{device!="lo"} [1m]) ) > 10 and ( rate(node_network_transmit_errs_total{device!="lo"} [1m]) ) > 1 and node_network_speed_bytes{device!="lo"}
  for: 2h
  labels:
    severity: critical
    priority: P1
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "device" }}  »  *{{ humanize (index $values "Value").Value }} % Error*
    description: Host **{{ index $labels "instance" }}** network interface **{{ index $labels "device" }}** has encountered transmit errors.\nValue = {{ humanize (index $values "Value").Value }} % Error
```


## Host OS

### Host Anomalous Context Switch Rate (>3σ)

```yaml
- alert: HostAnomalousContextSwitchRate
  expr: abs( (avg_over_time(rate(node_context_switches_total{job="node"} [1m]) [1m:]) - avg_over_time(rate(node_context_switches_total{job="node"} [1m]) [24h:])) / stddev_over_time(rate(node_context_switches_total{job="node"} [1m]) [24h:]) ) > 3
  for: 15m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} σ*
    description: Host **{{ index $labels "instance" }}** context switch rate has been anomalous for the past 15 minutes.\nValue = {{ humanize (index $values "Value").Value }} σ
```

### Host Anomalous Interrupt Rate (>3σ)

```yaml
- alert: HostAnomalousInterruptRate
  expr: abs( (avg_over_time(rate(node_intr_total{job="node"} [1m]) [1m:]) - avg_over_time(rate(node_intr_total{job="node"} [1m]) [24h:])) / stddev_over_time(rate(node_intr_total{job="node"} [1m]) [24h:]) ) > 3
  for: 15m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }} σ*
    description: Host **{{ index $labels "instance" }}** interrupt rate has been anomalous for the past 15 minutes.\nValue = {{ humanize (index $values "Value").Value }} σ
```

### Host Clock Not Synchronised

```yaml
- alert: HostClockNotSynchronized
  expr: ( node_timex_sync_status{job="node"} ) < 1
  for: 15m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}
    description: Host **{{ index $labels "instance" }}** clock is not synchronising for the past 15 minutes.
```

```yaml
- alert: HostClockNotSynchronized
  expr: ( node_timex_sync_status{job="node"} ) < 1
  for: 24h
  labels:
    severity: warning
    priority: P3
  annotations:
    summary: {{ index $labels "instance" }}
    description: Host **{{ index $labels "instance" }}** clock is not synchronising for the past 24 hours.
```

### Host Clock Skewed (>50ms)

```yaml
- alert: HostClockSkewed
  expr: abs( node_timex_offset_seconds{job="node"} ) > 0.05
  for: 15m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }}*
    description: Host **{{ index $labels "instance" }}** clock skew is above 50ms for the past 15 minutes.\nValue = {{ humanize (index $values "Value").Value }}
```

```yaml
- alert: HostClockSkewed
  expr: abs( node_timex_offset_seconds{job="node"} ) > 0.05 
  for: 24h
  labels:
    severity: warning
    priority: P3
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }}*
    description: Host **{{ index $labels "instance" }}** clock skew is above 50ms for the past 24 hours.\nValue = {{ humanize (index $values "Value").Value }}
```

### Host Update Required

```yaml
- alert: HostUpdateRequired
  expr: ( sum by (instance) (apt_upgrades_held{job="node"}) ) > 0
  for: 15m
  labels:
    severity: info
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }}*
    description: Host **{{ index $labels "instance" }}** can be updated.\nValue = {{ humanize (index $values "Value").Value }}
```

```yaml
- alert: HostUpdateRequired
  expr: ( sum by (instance) (apt_upgrades_held{job="node"}) ) > 0
  for: 7d
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }}*
    description: Host **{{ index $labels "instance" }}** can be updated.\nValue = {{ humanize (index $values "Value").Value }}
```

### Host Reboot Required

```yaml
- alert: HostRebootRequired
  expr: ( node_reboot_required{job="node"} ) > 0
  for: 24h
  labels:
    severity: info
  annotations:
    summary: {{ index $labels "instance" }}
    description: Host **{{ index $labels "instance" }}** requires reboot - running outdated kernel.
```

```yaml
- alert: HostRebootRequired
  expr: ( node_reboot_required{job="node"} ) > 0
  for: 7d
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}
    description: Host **{{ index $labels "instance" }}** requires reboot - running outdated kernel.
```

### Host Low Entropy (<256b)

```yaml
- alert: HostLowEntropy
  expr: ( min_over_time(node_entropy_available_bits{job="node"} [1m]) ) < 256
  for: 5m
  labels:
    severity: warning
    priority: P4
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }}*
    description: Host **{{ index $labels "instance" }}** available entropy is below 256b for the last 5 minutes.\nValue = {{ humanize (index $values "Value").Value }}
```

```yaml
- alert: HostLowEntropy
  expr: ( min_over_time(node_entropy_available_bits{job="node"} [1m]) ) < 256
  for: 2h
  labels:
    severity: warning
    priority: P3
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }}*
    description: Host **{{ index $labels "instance" }}** available entropy is below 256b for the last 2 hours.\nValue = {{ humanize (index $values "Value").Value }}
```


## Host Service

### Host Service Flapping

```yaml
- alert: HostServiceFlapping
  expr: ( increase(node_systemd_service_restart_total{job="node"} [1m]) ) > 1
  for: 5m
  labels:
    severity: critical
    priority: P2
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "name" }}
    description: Host **{{ index $labels "instance" }}** service(systemd) **{{ index $labels "name" }}** is flapping for the past 5 minutes.
```

```yaml
- alert: HostServiceFlapping
  expr: ( increase(node_systemd_service_restart_total{job="node"} [1m]) ) > 1
  for: 2h
  labels:
    severity: critical
    priority: P1
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "name" }}
    description: Host **{{ index $labels "instance" }}** service(systemd) **{{ index $labels "name" }}** is flapping for the past 2 hours.
```

### Host Service Failed

```yaml
- alert: HostServiceFailed
  expr: ( node_systemd_unit_state{job="node",state="failed"} ) > 0
  for: 30s
  labels:
    severity: critical
    priority: P2
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "name" }}
    description: Host **{{ index $labels "instance" }}** service(systemd) **{{ index $labels "name" }}** is down for the past 30 seconds.
```

```yaml
- alert: HostServiceFailed
  expr: ( node_systemd_unit_state{job="node",state="failed"} ) > 0
  for: 10m
  labels:
    severity: critical
    priority: P1
  annotations:
    summary: {{ index $labels "instance" }}  »  {{ index $labels "name" }}
    description: Host **{{ index $labels "instance" }}** service(systemd) **{{ index $labels "name" }}** is down for the past 10 minutes.
```

### Host OOM Kill Invoked

```yaml
- alert: HostOomKillInvoked
  expr: ( increase(node_vmstat_oom_kill{job="node"} [1m]) ) > 0
  for: 0s
  labels:
    severity: critical
    priority: P2
  annotations:
    summary: {{ index $labels "instance" }}  »  *{{ humanize (index $values "Value").Value }}*
    description: Host **{{ index $labels "instance" }}** has invoked OOM kill in the past 30 seconds.\nValue = {{ humanize (index $values "Value").Value }}
```
