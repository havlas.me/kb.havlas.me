SOCKS5 SSH Tunnel
=================

```shell
ssh -D 1337 -q -C -N <username>@<hostname>
```
