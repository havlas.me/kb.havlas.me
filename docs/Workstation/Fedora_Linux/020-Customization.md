# Customization

## [GNOME Shell Extensions](https://extensions.gnome.org/)

* [Media Controls](https://extensions.gnome.org/extension/4470/media-controls/)
* [IdeaPad](https://extensions.gnome.org/extension/2992/ideapad/)

## [Orchis GTK Theme](https://github.com/vinceliuice/Orchis-theme.git)

```bash
cd /tmp
git clone https://github.com/vinceliuice/Orchis-theme.git
cd Orchis-theme
./install.sh -t red -c dark -s standard --tweaks submenu
#cp -r src/firefox/chrome/ ~/.mozilla/firefox/*.default-release/chrome/
#cp src/firefox/configuration/user.js ~/.mozilla/firefox/*.default-release/user.js
#echo 'user_pref("layers.acceleration.force-enabled", true)' >> ~/.mozilla/firefox/*.default-release/user.js
cd ..
rm -rf Orchis-theme
```

## [Tela Icon Theme](https://github.com/vinceliuice/Tela-icon-theme)

```bash
cd /tmp
git clone https://github.com/vinceliuice/Tela-icon-theme.git
cd Tela-icon-theme
./install.sh red
cd ..
rm -rf Tela-icon-theme
```

## [Bibata Cursor Theme](https://github.com/ful1e5/Bibata_Cursor)

[test](../../_dl/Bibata-Modern-067b0-v1.0.1-x11.tar.gz)
[live](https://www.bibata.live/studio)

```bash
cd /tmp
wget https://kb.havlas.me/_dl/Bibata-Modern-067b0-v1.0.1-x11.tar.gz
tar xvf Bibata-Modern-*.tar.gz
mv Bibata-*/ ~/.local/share/icons/Bibata-Modern/
rm Bibata-Modern-*.tar.gz
```

## [Tela GRUB Theme](https://github.com/vinceliuice/grub2-themes.git)

```bash
cd /tmp
git clone https://github.com/vinceliuice/grub2-themes.git
cd grub2-themes
sudo ./install.sh -t tela -i white -b
```

### Fix `/boot/efi/EFI/fedora/grub.cfg`

```bash
sudo dnf reinstall shim-* grub2-efi-* grub2-common
```
