# Installation

## Enable [RPM Fusion](https://rpmfusion.org/Configuration)

```bash
sudo dnf install \
  https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \
  https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
```

## Base

```bash
sudo dnf install \
  chromium \
  curl \
  ethtool \
  firefox \
  gnome-tweaks \
  pipx \
  rsync \
  screen \
  wget
```

## Development

```bash
sudo dnf install \
  ShellCheck \
  ansible \
  bmap-tools \
  cmake \
  dos2unix \
  git \
  jq \
  make \
  podman \
  python3-devel \
  qrencode \
  sqlite
```
