# GitLab Repository Mirroring to GitHub

1. Create a GitHub Repository  
   <https://github.com/new>

2. Create a GitHub Personal Access Token  
   <https://github.com/settings/tokens>

3. Go to GitLab Project Settings > Repository > Mirroring repositories  
   <https://gitlab.com/havlas.me/**REPONAME**/-/settings/repository>

4. Create a Repository Mirroring  
   *Git repository URL:* https://github.com/havlasme/**REPONAME**.git  
   *Username:* havlasme  
   *Password:* GitHub Personal Access Token  
