Git Base Usage
==============

Amend the Last Commit Date
--------------------------

```bash
git commit --amend --date="$(date -R)"
```
