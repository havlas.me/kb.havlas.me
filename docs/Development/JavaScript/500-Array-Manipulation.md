Array Manipulation
==================

Omitting nth element from an array
----------------------------------

```javascript title="Omit second element"
const state = [1, 2, 3, 4, 5]
const nextState = [...state]
nextState.splice(2, 1)
```
