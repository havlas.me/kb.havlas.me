Bootstrap a Machine
===================

Install a Base Linux Tool-Set
-----------------------------

```shell
sudo apt update
sudo apt install -y at bzip2 curl dnsutils screen unzip vim wget
sudo apt install -y ethtool hdparm htop iotop smem 
sudo apt install -y bash-completion debsums libpam-pwquality libpam-tmpdir 
```

Set the Linux HostName
----------------------

```shell
sudo hostnamectl set-hostname <hostname>
```

Set the Linux TimeZone
----------------------

```shell
sudo timedatectl set-timezone <timezone>
```

### Set the Linux TimeZone to `UTC`

```shell
sudo timedatectl set-timezone Etc/UTC
```

Generate the Linux Locales
--------------------------

```shell
TODO: add locale to /etc/locale.gent
sudo locale-gen
```

### Generate the `en_US.UTF-8` and `sk_SK.UTF-8` Locale

```shell
TODO
sudo locale-gen
```

Tune the Linux Kernel Tunable Variable
--------------------------------------

```shell
TODO: add kernel variables to /etc/sysctl.conf
```

Setup Time Synchronization via NTP/NTS
--------------------------------------

```shell
sudo apt install -y chrony
TODO: configure chrony
sudo systemctl restart chronyd
```
