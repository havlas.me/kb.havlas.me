Install the Molly Guard
=======================

Install the Molly Guard to prevent accidental shutdown and reboot.

```shell
sudo apt update
sudo apt install -y molly-guard
```
