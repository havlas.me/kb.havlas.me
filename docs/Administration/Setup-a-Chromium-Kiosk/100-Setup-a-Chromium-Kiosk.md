Setup a Chromium Kiosk
======================

```shell
sudo apt install -y chromium xinit
```

```shell
sudo useradd -d /var/lib/chromium -s /bin/bash --create-home --system chromium
sudo passwd -l chromium
sudo gpasswd -a chromium video
sudo gpasswd -a chromium input
```

```shell
cat <<EOF | sudo tee /etc/udev/rules.d/99-tty1-chromium.rules > /dev/null
KERNEL=="tty1",OWNER="chromium"
EOF
```

```shell
TODO edit /etc/X11/Xwrapper.config
allowed_users=anybody
```

```shell
cat <<EOF | sudo tee /etc/systemd/system/x-chromium.service > /dev/null
[Unit]
Description=Chromium Kiosk
After=network.target
Conflicts=getty@tty1.service

[Service]
Type=simple
User=chromium
Group=chromium
EnvironmentFile=/etc/default/chromium
ExecStart=/usr/bin/xinit /usr/bin/chromium \$CHROMIUM_OPTS -- :0 vt1 -v -s 0 -dpms -nocursor
Restart=always

[Install]
WantedBy=multi-user.target
EOF
```

```shell
cat <<EOF | sudo tee /etc/default/chromium > /dev/null
CHROMIUM_OPTS="--kiosk --window-size=1920,1080 --window-position=0,0 --start-fullscreen --noerrdialogs --disable-infobars <WEBSITE>"
EOF
```

```shell
sudo sudo systemctl daemon-reload
sudo systemctl mask getty@tty1.service
sudo systemctl enable x-chromium.service 
```
