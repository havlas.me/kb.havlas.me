# Authoring Guideline

## Naming the Variables

* `<rolename>__state` : `enum('present', 'latest', 'absent') | d('present')`

  Role state, value should be one of the [ 'present', 'latest', 'absent' ]. Based on this value the role should
  install, update or uninstall the service (package) and all related configuration.

* `<rolename>__enabled` : `boolean | d(true)`

  Service state, based on this value the role should enable or disable the systemd service - ensure it is started  
  or stopped at boot. Should default to `false` if role state is `absent`.

* `<rolename>__ansible_reload` : `boolean | d(true)`

  Use this variable to control if the role can reload the systemd service.

* `<rolename>__ansible_restart` : `boolean | d(true)`

  Use this variable to control if the role can restart the systemd service.

* `<rolename>__user` : `string`

  Service user, the role should create this user if it does not exist and ensure the service is running as this user.

* `<rolename>__group` : `string`

  Service group, the role should create this group if it does not exist and ensure the service is running as this group.

* `<rolename>__port` : `int|int[]`

* `<rolename>__bind_to` : `string|string[]`

* `<rolename>__conf` : `dict[]`

* `<rolename>__conf_template` : `string`

* `<rolename>__confdir` : `string`

* `<rolename>__datadir` : `string`

## Common Install Task-Set

```yaml title="Install / Update / Uninstall via APT"
- name: 'Install | Install, Update, or Uninstall the <rolename> via APT'
  ansible.builtin.apt:
    name: '{{ <rolename>__package_name }}'
    update_cache: '{{ <rolename>__state != "absent" }}'
    cache_valid_time: 1800
    state: '{{ <rolename>__state }}'
  notify:
  - '<rolename> | Restart Service'
  tags: [ 'role:<rolename>', 'role:<rolename>:install', 'task:install' ]
```

```yaml title="Create the Service Group"
- name: 'Install | Create the <rolename> Service Group'
  ansible.builtin.group:
    name: '{{ <rolename>__runas_group }}'
    system: true
  notify:
  - '<rolename> | Restart Service'
  when:
  - <rolename>__runas_group != 'root'
  - <rolename>__state != 'absent'
```

```yaml title="Create the Service User"
- name: 'Install | Create the <rolename> Service User'
  ansible.builtin.user:
    name: '{{ <rolename>__runas_user }}'
    password_lock: true
    shell: '/sbin/nologin'
    home: '/nonexistent'
    create_home: false
    system: true
  notify:
  - '<rolename> | Restart Service'
  when:
  - <rolename>__runas_user != 'root'
  - <rolename>__state != 'absent'
```

## Common Service Task-Set

```yaml title="Start/Stop and Enable/Disable the Service"
- name: 'Service | {{ (chrony__state == "absent") | ternary("Stop and Disable", "Start and Enable") }} the <rolename> Service'
  ansible.builtin.systemd:
    name: '{{ <rolename>__service_name }}'
    daemon_reload: true
    enabled: '{{ (<rolename>__state == "absent") | ternary(false, <rolename>__enabled) }}'
    state: '{{ (<rolename>__state == "absent") | ternary("stopped", "started") }}'
  tags: [ 'role:<rolename>', 'role:<rolename>:service', 'task:service' ]
```

```yaml title="Reload the Service"
- name: '<rolename> | Reload Service'
  ansible.builtin.systemd:
    name: '{{ <rolename>__service_name }}'
    daemon_reload: true
    state: 'reloaded'
  when:
  - <rolename>__ansible_reload | bool
  - <rolename>__state != 'absent'
  - not ansible_check_mode
  tags: [ 'always' ]
``` 

```yaml title="Restart the Service"
- name: '<rolename> | Restart Service'
  ansible.builtin.systemd:
    name: '{{ <rolename>__service_name }}'
    daemon_reload: true
    state: 'restarted'
  when:
  - <rolename>__ansible_restart | bool
  - <rolename>__state != 'absent'
  - not ansible_check_mode
  tags: [ 'always' ]
```

## Assert Managed Node is Supported

```yaml title="Assert that the Managed Node OS is Supported"
- name: 'Assert that the Managed Node OS is Supported'
  ansible.builtin.assert:
    that:
    - (ansible_distribution|lower == "debian" and ansible_distribution_version|int in [10, 11, 12]) or
      (ansible_distribution|lower == "ubuntu" and ansible_distribution_version|int in [20, 22, 24])
    msg: 'The "{{ ansible_distribution }} {{ ansible_distribution_version }}" is not supported'
    quiet: true
  tags: [ 'always' ]
```

```yaml title="Assert that the Managed Node is Running systemd"
- name: 'Assert that the Managed Node is Running systemd'
  ansible.builtin.assert:
    that:
    - ansible_service_mgr == 'systemd'
    msg: 'The "{{ ansible_service_mgr }}" is not supported'
    quiet: true
  tags: [ 'always' ]
```

```yaml title="Assert that the Role State Value is Valid"
- name: 'Assert that the <rolename> State Value is Valid'
  ansible.builtin.assert:
    that:
    - <rolename>__state in [ 'present', 'latest', 'absent' ]
    msg: 'The `<rolename>__state` must be one of [ "present", "latest", "absent" ] - "{{ <rolename>__state }}" is not supported'
    quiet: true
  tags: [ 'always' ]
```
