# [kb.havlas.me](https://kb.havlas.me/)

Personal knowledge base by Tomáš Havlas.


## Build

```shell
make clean build
```


## Development

```shell
make dev
```

### Prepare Development Environment

```shell
pip install --upgrade mkdocs-material
```
